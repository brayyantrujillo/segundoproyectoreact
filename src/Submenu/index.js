import React from "react";
import styled from 'styled-components';
import Options  from "./options";
import Footer  from "./footer";




const Container = styled.div`
margin-top: 15px;
background-color: ${props => props.color};
border-radius:25px;
padding:15px;
width:400px;
padding-top:15px;

//opacity:0.8;

`

const App=(params)=> {

  /**
   * color:string;
   * options: string[]
   */
   
 const handleChange = (value) =>{

    if(typeof params.onChange === "function") params.onChange(value, params.id);

}

const handleReset = () =>{

  if(typeof params.onReset === "function") params.onReset(params.id);

}

  return (
    <Container color={params.color}>

        {params.options.map((v, i) => <Options key={i} {...v} onChange = {handleChange}/>)}
         
        

         <Footer version={params.footer} onReset={handleReset}  /*onReset onCancel onAcep */ /> 
    </Container>
  );
};

export default App;

