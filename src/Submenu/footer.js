import React from "react";
import styled from 'styled-components';

import { Circulo, Icon } from "./options";

import check from '../imagenes/check.jpg';
import cancel from '../imagenes/cruz.png';
import reset from '../imagenes/reset.png';

const Container = styled.div`
display: flex;
justify-content: center;
align-items: center;
flex-direction: row;
`;
const ContainerV1 = styled.div`
display: flex;
justify-content: center;
align-items: center;
flex-direction: row;
margin-top: 15px;
padding-top: 25px;
padding-bottom: 25px;
border-Top: 1px solid #ffffff80;
`;
const Item = ({ v, onClick }) => {
    return(
        <Circulo visible ={true} style={{marginleft: 15, height: 60,width:60}} onClick ={onClick}> 
           <Icon visible ={true} src={v} style = {{height:60,width:60}}/>
        </Circulo>

    )

};

const App =({ version, onReset,onCancel,onAcept}) => {

    const handleReset = () =>{
        console.log("vamos a resetear")

        if(typeof onReset === "function") onReset()
    }
    const handleOk = () =>{
        console.log("vamos a aceptar")
        if(typeof onReset === "function") onAcept()

    }
    const handleCancel = () =>{
        console.log("vamos a cancelar")
        if(typeof onReset === "function") onCancel()
    }


  if (version === 1) {

     let data = [
          { icon: cancel, onClick: handleCancel },
          { icon: reset, onClick: handleReset },
          { icon: check, onClick: handleOk }

     ]; 

    return (
        <ContainerV1>
                                  
               
       
               {data.map((v, i) => <Item v= {v.icon} onClick ={v.onClick}/>)}
               
               
    
            
        </ContainerV1>
      );
  }

  if(version === 2){
     
    let data =[
        {icon: cancel, onClick: handleCancel},
        {icon: check, onClick: handleOk},

   ];


    return (
        <Container >

               {data.map((v, i) => <Item v= {v.icon} onClick = {v.onClick} /> )}
 
        </Container>
      );

  }    
         return <></>

    };
    


export default App;

